package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DurationTest {

    public DurationTest() {
    }

    @Test
    public void testDur() {
        final Duration duration = new Duration();
        assertEquals("test add", "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", duration.dur());
    }

    @Test
    public void testCalculateIntValue() {
        final Duration duration = new Duration();
        assertEquals("test calculate int", 5, duration.calculateIntValue());
    }
}

